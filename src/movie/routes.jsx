import React from 'react';
import { Route, Switch } from "react-router-dom";

import Home from './Home';
import Login from './Login';
import MovieListEditor from './editor/MovieListEditor';
import About from './About';

const routes = (
    <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/movie" component={MovieListEditor} />
        <Route path="/about" component={About} />
    </Switch>
)

export default routes;
