import React, { Component } from 'react';
import { Link } from "react-router-dom";

import Client from '../services/ApiServices'

class MovieItem extends Component {
    constructor(){
        super();
    }

    render = () => (
        <div>
            <Link to="#">
                <h3>{this.props.data.title}</h3>
            </Link>
            <p>
                {this.props.data.description}
            </p>
        </div>
    )
}

class Home extends Component {
    constructor(){
        super();
        this.state = {
            movies: []
        }
    }

    componentDidMount() {
        document.title = "Home"
        this.fetchMovies();
    }

    fetchMovies() {
        Client.getAll()
          .then(response => {
            console.log(response.data);
            this.setState({
                movies: response.data
            });
          })
          .catch(e => {
            console.log(e);
          });
    }

    render(){
        if (this.state.movies.length === 0) {
            return <h1>Tidak ada Film</h1>
        }

        return (
            <div>
                <h1>Daftar Film</h1>
                {
                    this.state.movies
                    .sort((a, b) => a.rating < b.rating ? 1 : -1)
                    .map( (movie, i) => 
                    <MovieItem 
                        key={i} 
                        data={movie} />
                    )
                }
            </div>
        )
    }
}

export default Home
