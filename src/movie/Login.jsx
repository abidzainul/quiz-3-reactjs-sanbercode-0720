import React, { useContext, useEffect, useState } from 'react';
import { SessionContext } from './SessionProvider';


const Login = () => {
    const [session, setSession] = useContext(SessionContext);
    const [login, setLogin] = useState ({
        username: "admin",
        password: "admin",
    })
    const [errors, setErrors] = useState("Username atau Password Salah!!")


    useEffect(() => {
        document.title = "Login"
    }, []);
    
    const handleChange = (event) => {
        const value = event.target.value;
        setLogin({
            ...login,
            [event.target.name]: value
        });
    }

    const handleValidation = () => {
        let valid = true;
        let error = ""

        if(login.username !== "admin"){
           valid = false;
           error = error.concat("Username Salah\n");
        }

        if(login.password !== "admin"){
           valid = false;
           error = error.concat("Password Salah\n");
        }

        setErrors(error);
        return valid;
   }

    const handleSubmit = (event) => {
        event.preventDefault()

        if(handleValidation()){
            setSession(login)
        }else{
            console.log(errors)
            alert(errors)
        }
    }

    return (
        <div className="login">
            <h1>Login</h1>
            <form onSubmit={handleSubmit}>
                <div className="row-login">
                    <label> Username </label>          
                    <input type="text" 
                        name="username"
                        value={login.username}
                        onChange={handleChange}/>
                </div>
                <div className="row-login">
                    <label> Password </label>          
                    <input type="password" 
                        name="password"
                        value={login.password}
                        onChange={handleChange}/>
                </div>
                <input type="submit" value="Submit" />
            </form>
        </div>
    )
}

export default Login