import React from 'react';
import logo from '../assets/img/logo.png';
import '../assets/css/style.css';
import { BrowserRouter as Router } from "react-router-dom";
import { SessionProvider } from './SessionProvider';

import routes from './routes';
import Nav from './Nav';

const Movie = () => (
    <SessionProvider>
        <Router>
            <div className="App">
                <header>
                    <img id="logo" src={logo} width="200px" />
                    <Nav />
                </header>

                <section >
                    {routes}
                </section>

                <footer>
                    <h5>copyright &copy; 2020 by Sanbercode</h5>
                </footer>
            </div>
        </Router>
    </SessionProvider>
)

export default Movie;