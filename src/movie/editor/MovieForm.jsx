import React, { useState, useEffect, useContext } from 'react';
import client from '../../services/ApiServices';
import { MovieContext } from './MovieProvider';

const MovieForm = () => {
    const { list, data, update } = useContext(MovieContext);
    const [listMovie, setListMovie] = list
    const [movie, setMovie] = data
    const [isUpdate, setIsUpdate] = update
    const [errors, setErrors] = useState("Field tidak boleh kosong!!")

    const fetchMovies = () => {
        client.getAll()
        .then(res => {
            console.log(res.data);
            setListMovie(res.data);
        }).catch(e => {
            console.log(e)
        })
    }

    const handleChange = (event) => {
        const value = event.target.value;
        setMovie({
            ...movie,
            [event.target.name]: value
        });
    }

    const handleValidation = () => {
        let valid = true;
        let error = ""

        if(movie.title===""){
           valid = false;
           error = error.concat("Judul tidak boleh kosong\n");
        }

        if(movie.description===""){
           valid = false;
           error = error.concat("Deskripsi tidak boleh kosong\n");
        }

        if(movie.year===""){
           valid = false;
           error = error.concat("Tahun tidak boleh kosong\n");
        }

        if(movie.duration===""){
           valid = false;
           error = error.concat("Durasi tidak boleh kosong\n");
        }

        if(movie.genre===""){
           valid = false;
           error = error.concat("Genre tidak boleh kosong\n");
        }

        if(movie.rating===""){
           valid = false;
           error = error.concat("Rating tidak boleh kosong\n");
        }

        setErrors(error);
        return valid;
   }

    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(movie)

        if(handleValidation()){
            if(isUpdate){
                client.update(movie.id, movie)
                    .then(res => {
                        console.log(`update: ${res}`)
                        reset()
                    }).catch(e => {
                        console.log(`error: ${e}`)
                    })
            }else {
                client.insert(movie)
                    .then(res => {
                        console.log(`insert: ${res}`)
                        reset()
                    }).catch(e => {
                        console.log(`error: ${e}`)
                    })
            }
        }else{
            console.log(errors)
            alert(errors)
        }
    }

    const reset = () => {
        setMovie({
            id: null,
            title: "",
            description: "",
            year: null,
            duration: 0,
            genre: "",
            rating: 1,
        })

        setIsUpdate(false)
        fetchMovies()
    }
    
    return (
        <div>
            <h1>Form Input</h1>
            <form onSubmit={handleSubmit}>
                <div className="row">
                    <div className="column">
                        <div className="column">
                            <label>Judul</label>          
                            <input type="text" 
                                name="title"
                                value={movie.title}
                                onChange={handleChange}/>
                        </div>
                        <div className="column">
                            <label>Deskripsi</label>          
                            <textarea
                                name="description"
                                value={movie.description}
                                onChange={handleChange}/>
                        </div>
                        <div className="column">
                            <label>Tahun</label>        
                            <input type="number" 
                                name="year"
                                min="1000" 
                                max="3000"
                                value={movie.year}
                                onChange={handleChange}/>
                        </div>
                    </div>

                    
                    <div className="column">
                        <div className="column">
                            <label>Durasi (Menit)</label>        
                            <input type="number" 
                                name="duration"
                                value={movie.duration}
                                onChange={handleChange}/>
                        </div>
                        <div className="column">
                            <label>Genre</label>        
                            <input type="text" 
                                name="genre"
                                value={movie.genre}
                                onChange={handleChange}/>
                        </div>
                        <div className="column">
                            <label>Rating</label>        
                            <input type="number" 
                                name="rating"
                                min="1" 
                                max="10"
                                value={movie.rating}
                                onChange={handleChange}/>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <input type="submit" value="Submit" />
                </div>
            </form>
        </div>
    )
}

export default MovieForm