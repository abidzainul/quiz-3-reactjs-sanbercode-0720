import React, { useEffect, useContext } from 'react';
import client from '../../services/ApiServices';
import { MovieContext } from './MovieProvider';

import TableItem from './TableItem';

const TableTitle = () => {
    return(
        <thead>
            <tr>
                <th style={{width: 100}}>Judul</th>
                <th style={{width: 200}}>Deskripsi</th>
                <th style={{width: 30}}>Tahun</th>
                <th style={{width: 30}}>Durasi</th>
                <th style={{width: 100}}>Genre</th>
                <th style={{width: 30}}>Rating</th>
                <th style={{width: 30}}>Action</th>
            </tr>
        </thead>
    )
}

const MovieTable = () => {
    const { list, data, update } = useContext(MovieContext);
    const [listMovie, setListMovie] = list
    const [movie, setMovie] = data
    const [isUpdate, setIsUpdate] = update
    

    useEffect(() => {
        fetchMovies();
      }, []);

    const fetchMovies = () => {
        client.getAll()
        .then(res => {
            console.log(res.data);
            setListMovie(res.data);
        }).catch(e => {
            console.log(e)
        })
    }
    
    const onEdit = (id, event) => {
        console.log(id)
        let data = listMovie.find(movie => movie.id === id)
        
        console.log(data);
        setMovie(data)

        setIsUpdate(true)
    }
    
    const onDelete = (id, event) => {
        event.preventDefault()
        console.log(id)
        client.remove(id)
            .then(res => {
                console.log("delete")
                fetchMovies()
            }).catch(e => {
                console.log(e)
            })
        
        setIsUpdate(false)
    }
    
    return (
        <div>
            <h1>Table Daftar Film</h1>
            <table className="movies">
                <TableTitle />
                <tbody>
                    {
                        listMovie
                        .sort((a, b) => a.rating < b.rating ? 1 : -1)
                        .map( (movie, i) => 
                            <TableItem key={i} 
                            data={movie} 
                            onEdit={onEdit} 
                            onDelete={onDelete} 
                            index={i}/>
                        )
                    }
                </tbody>
            </table>
        </div>
    )

}

export default MovieTable