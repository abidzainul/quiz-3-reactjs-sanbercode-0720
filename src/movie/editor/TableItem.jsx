import React from 'react';

const TableItem = (props) => {

    const onEdit = (id, e) => {
        e.preventDefault();
        props.onEdit(id, e)
    }
    
    const onDelete = (id, e) => {
        e.preventDefault();
        props.onDelete(id, e)
    }

    return (
        
        <tr>
            <td>{props.data.title}</td>
            <td>{props.data.description}</td>
            <td>{props.data.year}</td>
            <td>{props.data.duration}</td>
            <td>{props.data.genre}</td>
            <td>{props.data.rating}</td>
            <td>
                <button onClick={(e) => onEdit(props.data.id, e)}>Edit</button>
                <button style={{backgroundColor: "#C22"}} onClick={(e) => onDelete(props.data.id, e)}>Delete</button>
            </td>
        </tr>
    )
}

export default TableItem