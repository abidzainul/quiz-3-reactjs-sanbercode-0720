import React, { useEffect, useContext } from 'react';

import { MovieProvider } from './MovieProvider';
import MovieForm from './MovieForm'
import MovieTable from './MovieTable'
import { SessionContext } from '../SessionProvider';
import Login from '../Login'

const MovieListEditor = () => {
    const [session, setSession] = useContext(SessionContext);

    useEffect(() => {
        document.title = "About"
        console.log(session.username)
     }, []);
    
    if (session.username === null) {
        return (
            <div>
                <h1>Anda Belum Login</h1>
                <h1>Login untuk melanjutkan ke fitur ini</h1>
                <Login />
            </div>
        )
    }

    return (
        <MovieProvider>
            <MovieForm/>
            <MovieTable/>
        </MovieProvider>
    )
}

export default MovieListEditor