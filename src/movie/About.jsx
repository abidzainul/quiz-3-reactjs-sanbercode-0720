import React, { useEffect } from 'react';

const About = () => {
    useEffect(() => {
        document.title = "About"
     }, []);

    return (
        <div style={{padding: "10px", border: "1px solid #ccc"}}>
        <h1 style={{textAlign: "center"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
            <ol>
                <li><strong style={{width: "100px"}}>Nama:</strong> Ahmad Zainul Abidin</li> 
                <li><strong style={{width: "100px"}}>Email:</strong> abidzainul94@gmail.com</li> 
                <li><strong style={{width: "100px"}}>Sistem Operasi yang digunakan:</strong> Windows 10</li>
                <li><strong style={{width: "100px"}}>Akun Gitlab:</strong> @abidzainul</li> 
                <li><strong style={{width: "100px"}}>Akun Telegram:</strong> 081946224450</li> 
            </ol>
        </div>
    )
}

export default About