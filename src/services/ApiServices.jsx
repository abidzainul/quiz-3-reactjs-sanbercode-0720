import http from './ApiConfig';

const getAll = () => {
    return http.get(`/movies`)
}

const get = id => {
    return http.get(`/movies?id=${id}`)
}

const insert = data => {
    return http.post(`/movies`, data)
}

const update = (id, data) => {
    return http.put(`/movies/${id}`, data)
}

const remove = id => {
    return http.delete(`/movies/${id}`)
}
  
export default {
    getAll,
    get,
    insert,
    update,
    remove
};